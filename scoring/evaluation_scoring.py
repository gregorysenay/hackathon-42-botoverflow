import sys
import csv
import numpy
from scipy import stats

def get_data(filename):
    data = open(filename, 'r')
    reader = csv.reader(data, delimiter=',')
    next(reader, None)
    all_data = [] 
    for r in reader:
        l = numpy.array([ int(v) for v in r] )
        all_data.append(l)
    return numpy.array(all_data)

def score(hyp_data, ref_data, index_beg, index_end, index_ref):
    hyp = hyp_data[:,index_beg:index_end]
    ref = ref_data[:,index_ref]
    Pat3 = 0.
    Pat1 = 0.
    for i in range(hyp.shape[0]):
        if ref[i] in hyp[i]:
            Pat3 += 1.
        if ref[i] == hyp[i][0]:
            Pat1 += 1.
    Pat1 /= float(hyp.shape[0]) 
    Pat3 /= float(hyp.shape[0]) 
    return Pat1, Pat3
    
if __name__ == '__main__':

    if len(sys.argv) < 3:
        print("<hypothese> <reference>")
        exit()
    hyp = sys.argv[1]
    ref = sys.argv[2]

    hyp_data = get_data(hyp)
    ref_data = get_data(ref)
    assert hyp_data.shape[1] == 7
    assert ref_data.shape[1] == 3
    assert ref_data.shape[0] == hyp_data.shape[0]
    assert numpy.array_equal(hyp_data[:,0],ref_data[:,0]) # same qid

    search_p1, search_p3 = score(hyp_data, ref_data, 1, 4, 1)
    answer_p1, answer_p3 = score(hyp_data, ref_data, 4, 7, 2)
    
    print("Search score P@1: {:.2f} - P@3: {:.2f}".format(100*search_p1, 100*search_p3))
    print("Answer score P@1: {:.2f} - P@3: {:.2f}".format(100*answer_p1, 100*answer_p3))
    
    final_p1 = 100. * stats.hmean([search_p1, answer_p1]) if search_p1 > 0 and answer_p1 > 0 else 0
    final_p3 = 100. * stats.hmean([search_p3, answer_p3]) if search_p3 > 0 and  answer_p3 > 0 else 0
    print("Final Score  P@1: {:.2f} - P@3: {:.2f}".format(final_p1, final_p3))
    
