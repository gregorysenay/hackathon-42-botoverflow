# Auto Evaluation

## Development evaluation

This directory allows you to evaluate your system.
The script `evaluation_scoring.py` is computing the Precision@1 and Precision@3 (P@1 and P@3)
of the buckets and best answers.
[learn more about P@N](https://en.wikipedia.org/wiki/Evaluation_measures_(information_retrieval)#Precision_at_K)

The final score is the harmonic means of the P@1 and the P@3 of the buckets and best answers.

- `val.ref` contains the validation reference
- `val.sample` contains a random selection of the bucket ids and the best answer id.

You can run the scoring tool, by running:
`python evaluation_scoring.py val.sample val.ref`

The results must be:
```
Search score P@1: 0.01 - P@3: 0.03
Answer score P@1: 0.00 - P@3: 0.00
Final Score  P@1: 0.00 - P@3: 0.00
```

Higher the score are, better is your system.

## File explanation
Header of val.ref (mapping only the ids in val.xml, same thing for train.ref and train.xml):

- qid: is the question id
- g: is the best bucket id relative to qid
- bg: is the answer id relative to qid

Header of the val.sample / hypothesys file:

- qid: is the question id
- g1: is your 1st best bucket id
- g2: is your 2nd best bucket id
- g3: is your 3rd best bucket id
- bg1: is your 1st best answer id
- bg2: is your 2nd best answer id
- bg3: is your 3rd best answer id
